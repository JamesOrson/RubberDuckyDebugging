#ifndef RDDB_ASCII_ART
#define RDDB_ASCII_ART

#include <array>

namespace rddb
{
    static const std::array<const char*, 2> duck_frames = {
      R"(
                ,-.
            ,-'   '-,
          ,---' ~.)---,
          ; (((__   __)))
          ;  ((#)  ((#))
          |   \_/___\_/|
        ,"  ,-'    `__".
        (   ( ._   ____`.)--._        _
        `._ `-.`-'  \     _  `-. _,-' `-`.
          ,')   `.`._))  ,' `.   `.  ,',' `;
        .'  .     `--'  /     ).   `.     ;
      ;     `-        /     '  )         ;
      \                       ')       ,'
        \                     ,'      ;
        \               `~~~'       ,'
          `.                      _,'
            `.                ,--'
              `-._________,--'
      )",
      R"(
                ,-.
            ,-'   '-,
          ,---' ~.)---,
          ; (((__   __)))
          ;  ((#)  ((#))
          |   \_/___\_/|
        ,"  ,-'    `__".
        (   ( ._   ____`.)--._        _
        `._ `-.`- \__/-'  _  `-. _,-' `-`.
          ,')            ,' `.   `.  ,',' `;
        .'  .           /     ).   `.     ;
      ;     `-        /     '  )         ;
      \                       ')       ,'
        \                     ,'      ;
        \               `~~~'       ,'
          `.                      _,'
            `.                ,--'
              `-._________,--'
      )",
    };
}

#endif
