#ifndef RDDB_CLIENT
#define RDDB_CLIENT

#include "terminal.h"

namespace rddb
{
    enum struct TerminationStatus : int
    {
        Success = 0,
        Failure = 1,
        Uninitialized = 2,
    };

    class Client
    {
        public:
            Client()
            {
                this->initialized = false;
            }
            Client(const Client&) = delete;
            Client& operator= (const Client&) = delete;

            ~Client() = default;

            auto constexpr initialize() -> bool
            {
                if (this->terminal.initialize())
                {
                    this->initialized = true;
                }
                return this->is_initialized();
            }

            auto inline is_initialized() -> bool
            {
                return this->initialized;
            }

            auto run() -> TerminationStatus
            {
                if (!this->is_initialized())
                {
                    return TerminationStatus::Uninitialized;
                }

                while (true)
                {
                    switch (this->terminal.interact())
                    {
                        case InteractionResult::Continue:
                            continue;
                        case InteractionResult::Exit:
                            return TerminationStatus::Success;
                        case InteractionResult::Failure:
                            return TerminationStatus::Failure;
                        case InteractionResult::Uninitialized:
                            return TerminationStatus::Uninitialized;
                    }
                }
            }

        private:
            bool initialized;
            Terminal terminal;
    };
}

#endif
